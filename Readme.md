# Battleship
## Description
It's .Net 5 console application. 
It can be run in debug and release version. In debug all ships are visible on the board.
Program.cs contains definition for the game - there is configuration for number of ships and board size.

```cs
const int boardSize = 10;
int[] shipSizes = new int[] { 4, 4, 5 }
```
Ships positions are generated randomly, there is limited number of tries when computer tries to put ship. It's calculated according to he board size multiplied by 2 -> in average it should be enough to try put ship in all empty fields (once it's already occupied).

## Interaction
User should provide field position in the format letter-number (according to the board description). Example values A-1, D-5, E-10

## Game rules
It's commonly known battleship game. The game consists of the board (10x10) and ships (in size 4,4,5) which should be sunk by the user. User in each turn provides a field coordinates where to shoot. The game ends when all ships were sunk by the user.

### Author Krzysztof Krawiec
