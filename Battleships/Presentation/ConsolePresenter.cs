﻿using Battleships.Enums;
using Battleships.Interfaces;
using Battleships.Models;
using ConsoleTables;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleships.Presentation
{
    public class ConsolePresenter : IPresenter
    {
        public Position Read()
        {
            Console.WriteLine("Provide coordinate in format \"letter-digit\" (example A-1, B-3, D-6)");

            while (true)
            {
                var line = Console.ReadLine();
                var convertedValue = ConsoleAdapter.Read(line);
                if (convertedValue.HasValue)
                {
                    return convertedValue.Value;
                }
                Console.WriteLine("Invalid value. Provide coordinate in correct format \"letter-digit\" (example A-1, B-3, D-6)");
            }
        }
        public void PrintWinGame()
        {
            Console.WriteLine("You win the game !");
        }

        public void PrintBoard(Board board)
        {
            var fields = board.GetFields();

            var table = new ConsoleTable(GenerateHorizontalTableDescription(board.Size).ToArray());
            for (int i = 0; i < board.Size; ++i)
            {
                table.Rows.Add(Enumerable.Range(0, board.Size + 1).Select(s => "").ToArray());
            }

            foreach (var (position, field) in fields)
            {
                table.Rows[position.X][position.Y + 1] = MapFieldValueToString(field.Ship.HasValue, field.Status);
            }

            var verticalDescriptions = GenerateVerticalTableDescription(board.Size).ToArray();
            for (int i = 0; i < table.Rows.Count; ++i)
            {
                table.Rows[i][0] = verticalDescriptions[i];
            }

            table.Write(Format.Alternative);
        }

        private IEnumerable<string> GenerateHorizontalTableDescription(int boardSize)
        {
            yield return "";
            for (int i = 1; i <= boardSize; ++i)
            {
                yield return $"{i}";
            }
        }

        private IEnumerable<string> GenerateVerticalTableDescription(int boardSize)
        {
            return Enumerable.Range(0, boardSize).Select(x => $"{(char)('A' + x)}");
        }

        private string MapFieldValueToString(bool isVehicle, FieldStatus fieldStatus) =>
    (isVehicle, fieldStatus) switch
    {
        (true, FieldStatus.Visited) => "+",
        (false, FieldStatus.Visited) => "-",
        (true, FieldStatus.NotVisited) =>
#if DEBUG
"S",
#else
        "",
#endif
        _ => string.Empty
    };

        public void Notify(string notification)
        {
            Console.WriteLine(notification);
        }
    }
}
