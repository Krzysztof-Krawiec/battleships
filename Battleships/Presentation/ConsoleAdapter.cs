﻿using Battleships.Models;
using Hazzik.Maybe;

namespace Battleships.Presentation
{
    public static class ConsoleAdapter
    {
        private const int firstAsciLetter = 'A';
        public static Maybe<Position> Read(string input)
        {
            var splitted = input.ToUpper().Split("-");
            if (splitted.Length != 2)
            {
                return Maybe<Position>.Nothing;
            }
            if (splitted[0].Length != 1)
            {
                return Maybe<Position>.Nothing;
            }
            var x = splitted[0][0] - (char)firstAsciLetter;
            if (!int.TryParse(splitted[1], out var y))
            {
                return Maybe<Position>.Nothing;
            }

            return Position.Create(x, y - 1);
        }
    }
}
