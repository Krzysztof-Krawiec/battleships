﻿using Battleships.Exceptions;
using Battleships.Helpers;
using Battleships.Interfaces;
using Battleships.Models;
using Battleships.Presentation;

namespace Battleships
{
    public class Program
    {
        static void Main(string[] args)
        {
            const int boardSize = 10;
            int[] shipSizes = new int[] { 4, 4, 5 };
            var ships = ShipGenerator.GenerateShips(boardSize, shipSizes);
            var game = new Game(boardSize, ships);
            IPresenter presenter = new ConsolePresenter();
            do
            {
                presenter.PrintBoard(game.Board);
                try
                {
                    game.Shut(presenter.Read());
                    
                }
                catch (PositionOutsideTheBoard e)
                {
                    presenter.Notify(e.Message);
                }
                
            }
            while (!game.IsGameEnd());
            presenter.PrintWinGame();
        }
    }
}
