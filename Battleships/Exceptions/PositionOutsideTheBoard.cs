﻿using System;

namespace Battleships.Exceptions
{
    public class PositionOutsideTheBoard : Exception
    {
        public PositionOutsideTheBoard():base("Provided position outside the board")
        {
        }
    }
}
