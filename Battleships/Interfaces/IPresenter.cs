﻿using Battleships.Models;

namespace Battleships.Interfaces
{
    public interface IPresenter
    {
        Position Read();
        void PrintBoard(Board board);
        void PrintWinGame();
        void Notify(string notification);
    }
}
