﻿using Battleships.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace Battleships.Models
{
    public class Game
    {
        private readonly Board _board;
        private readonly IList<Ship> _ships = new List<Ship>();
        public Board Board { get => _board; }

        public Game(int gameSize, IList<Ship> ships)
        {
            _board = Board.CreateBoard(gameSize);
            _ships = ships;
            foreach (var ship in ships)
            {
                _board.AddShip(ship);
            }
        }

        public void Shut(Position coordinate)
        {
            var vehicle = _board.GetShipAtUnvisitedPositionAndMarkAsVisited(coordinate);
            if (vehicle.HasValue)
            {
                vehicle.Value.Damage(coordinate);
            }
        }

        public bool IsGameEnd()
        {
            return _ships.All(v => v.IsSunk);
        }
    }
}
