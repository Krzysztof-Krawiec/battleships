﻿using Battleships.Enums;
using Battleships.Exceptions;
using Hazzik.Maybe;
using System;
using System.Collections.Generic;
using static System.Linq.Enumerable;

namespace Battleships.Models
{
    public class Board
    {
        private const int boardStartPosition = 0;
        private readonly IDictionary<Position, Field> _fields;
        public int Size { get; private set; }

        private Board(IDictionary<Position, Field> fields, int size)
        {
            Size = size;
            _fields = fields;
        }

        public IReadOnlyList<(Position position, Field field)> GetFields()
        {
            return _fields.Select(x=>(x.Key,x.Value)).ToList();
        }

        public static Board CreateBoard(int size)
        {
            if (size <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(size));
            }

            IDictionary<Position, Field> fields = new Dictionary<Position, Field>();
            foreach (var index in Range(boardStartPosition, size * size))
            {
                fields.Add(Position.Create(index / size, index % size), new Field(Maybe<Ship>.Nothing, FieldStatus.NotVisited));
            }

            return new Board(fields, size);
        }

        public void AddShip(Ship ship)
        {
            if (ship == null)
            {
                throw new ArgumentNullException(nameof(ship));
            }

            EnsurePositionIsAvailable(ship.Positions.ToList());
            
            foreach (var position in ship.Positions)
            {
                _fields[position] = new Field(new Maybe<Ship>(ship), FieldStatus.NotVisited);
            }
        }

        private void EnsurePositionIsAvailable(IList<Position> coordinates)
        {
            foreach (var coordinate in coordinates)
            {
                if (_fields.TryGetValue(coordinate, out var field))
                {
                    if (field.Ship.HasValue)
                    {
                        throw new NotAvailablePositionForVehicle();
                    }
                }
                else
                {
                    throw new NotAvailablePositionForVehicle();
                }
            }
        }

        public Maybe<Ship> GetShipAtUnvisitedPositionAndMarkAsVisited(Position coordinate)
        {
            if (coordinate == null)
            {
                throw new ArgumentNullException(nameof(coordinate));
            }

            if (!_fields.TryGetValue(coordinate, out var field))
            {
                throw new PositionOutsideTheBoard();
            }

            if (field.Status == FieldStatus.Visited)
            {
                return Maybe<Ship>.Nothing;
            }

            field.MarkFieldAsVisited();
            _fields[coordinate] = field;
            return field.Ship;
        }
    }
}
