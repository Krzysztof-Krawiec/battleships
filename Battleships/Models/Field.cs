﻿using Battleships.Enums;
using Hazzik.Maybe;

namespace Battleships.Models
{
    public class Field
    {
        public Maybe<Ship> Ship { get; private set; }
        public FieldStatus Status { get; private set; }
        public Field(Maybe<Ship> ship, FieldStatus status)
        {
            Ship = ship;
            Status = status;
        }

        public void MarkFieldAsVisited()
        {
            Status = FieldStatus.Visited;
        }
    }
}
