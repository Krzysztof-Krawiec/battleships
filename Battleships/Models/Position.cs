﻿namespace Battleships.Models
{
    public class Position
    {
        public int X { get; }
        public int Y { get; }

        private Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Position Create(int x, int y)
        {
            return new Position(x, y);
        }
        public override int GetHashCode()
        {
            int hash = 23 * 37 + X.GetHashCode();
            hash = hash * 37 + Y.GetHashCode();

            return hash;
        }

        public override bool Equals(object obj)
        {
            var other = obj as Position;
            if (other is null)
            {
                return false;
            }

            return (X == other.X) && (Y == other.Y);
        }
      
        public static bool operator ==(Position first, Position second)
        {
            if (first is null)
            {
                if (second is null)
                {
                    return true;
                }
                return false;
            }
            return first.Equals(second);
        }
        public static bool operator !=(Position first, Position second) => !(first== second);
    }
}
