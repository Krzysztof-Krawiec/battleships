﻿using Battleships.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleships.Models
{
    public class Ship
    {
        private readonly IList<Position> Coordinates;
        public int Size { get => Coordinates.Count; }
        public bool IsSunk { get => Coordinates.Count == Damages.Count; }
        private IList<Position> Damages { get; }
        public IEnumerable<Position> Positions => Coordinates;

        private Ship(IList<Position> coordinates)
        {
            if (coordinates == null || coordinates.Count == 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            Damages = new List<Position>();
            Coordinates = coordinates;
        }

        public static Ship CreateShip(Position startPosition, int size, Direction direction)
        {
            if (startPosition == null)
            {
                throw new ArgumentOutOfRangeException();
            }
            IList<Position> shipPositions;
            switch (direction)
            {
                case Direction.Vertical:
                    shipPositions = Enumerable.Range(0, size).Select(counter => Position.Create(startPosition.X, startPosition.Y + counter)).ToList();
                    break;
                case Direction.Horizontal:
                    shipPositions = Enumerable.Range(0, size).Select(counter => Position.Create(startPosition.X + counter, startPosition.Y)).ToList();
                    break;
                default: throw new ArgumentOutOfRangeException(nameof(direction));
            }
            return new Ship(shipPositions);
        }

        public void Damage(Position coordinate)
        {
            if (coordinate == null)
            {
                throw new ArgumentNullException(nameof(coordinate));
            }
            if (!Coordinates.Contains(coordinate) || Damages.Contains(coordinate))
            {
                throw new ArgumentOutOfRangeException();
            }

            Damages.Add(coordinate);
        }
    }
}
