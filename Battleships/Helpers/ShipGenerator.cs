﻿using Battleships.Models;
using Battleships.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleships.Helpers
{
    public class ShipGenerator
    {
        private static readonly Random _random = new Random();
        public static IList<Ship> GenerateShips(int boardSize, int[] sizes)
        {
            int maxRandomRepeatTimes = boardSize * boardSize * 2;
            var result = new List<Ship>();

            foreach (var shipSize in sizes)
            {
                Ship ship;
                int counter = 0;
                do
                {
                    ship = GenerateShip(boardSize, shipSize);
                    ++counter;
                    if (counter == maxRandomRepeatTimes)
                    {
                        throw new Exception("Can not generate ship");
                    }
                } while (IsOverlapping(ship, result));
                result.Add(ship);
            }
            return result;
        }

        private static Ship GenerateShip(int boardSize, int shipSize)
        {
            if (_random.Next(0, 2) == 0)
            {
                var x = _random.Next(0, boardSize - shipSize + 1);
                var y = _random.Next(0, shipSize + 1);
                return Ship.CreateShip(Position.Create(x, y), shipSize, Direction.Vertical);
            }
            else
            {
                var x = _random.Next(0, shipSize + 1);
                var y = _random.Next(0, boardSize - shipSize + 1);
                return Ship.CreateShip(Position.Create(x, y), shipSize, Direction.Horizontal);
            }
        }

        private static bool IsOverlapping(Ship ship, IList<Ship> ships)
        {
            var allPositions = ships.Select(x => x.Positions).SelectMany(x => x);
            var shipPositions = ship.Positions;
            if (shipPositions.Any(x => allPositions.Contains(x)))
            {
                return true;
            }
            return false;
        }
    }
}
