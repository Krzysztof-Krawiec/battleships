﻿namespace Battleships.Enums
{
    public enum FieldStatus
    {
        NotVisited,
        Visited
    }
}
