﻿namespace Battleships.Enums
{
    public enum Direction
    {
        Vertical,
        Horizontal
    }
}
