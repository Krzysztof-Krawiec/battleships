using Battleships.Presentation;
using Xunit;

namespace UnitTests
{
    public class ConsoleAdapterTests
    {
        [Theory]
        [InlineData("A-1",0,0)]
        [InlineData("a-1",0,0)]
        [InlineData("B-1",1,0)]
        [InlineData("b-1",1,0)]
        [InlineData("C-3",2,2)]
        [InlineData("c-3",2,2)]
        public void Read_ReturnsExpectedPositionValues(string input, int expectedX, int expectedY)
        {
            var position = ConsoleAdapter.Read(input).Value;
            Assert.Equal(expectedX, position.X);
            Assert.Equal(expectedY, position.Y);
        }

        [Theory]
        [InlineData("A--1")]
        [InlineData("A1-1")]
        [InlineData("BB-1")]
        [InlineData("BB-1B")]
        [InlineData("1-B")]
        public void Read_ReturnsEmptyResultWhenInvalidValueOrFormat(string input)
        {
            var position = ConsoleAdapter.Read(input);
            Assert.False(position.HasValue);
        }
    }
}
