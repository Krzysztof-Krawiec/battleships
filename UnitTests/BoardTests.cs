﻿using Battleships.Enums;
using Battleships.Exceptions;
using Battleships.Models;
using Hazzik.Maybe;
using System;
using Xunit;

namespace UnitTests
{
    public class BoardTests
    {
        [Theory]
        [InlineData(5, 25)]
        [InlineData(2, 4)]
        [InlineData(6, 36)]
        public void CreateBoard_CreatesBoardWithExpectedNumberOfFields(int boardSize, int expectedFieldsNumber)
        {
            var _sut = Board.CreateBoard(boardSize);

            Assert.Equal(expectedFieldsNumber, _sut.GetFields().Count);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-5)]
        [InlineData(-1)]
        public void CreateBoard_ThrowsException_WhenInvalidBoardSizeProvided(int boardSize)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Board.CreateBoard(boardSize));
        }

        [Fact]
        public void AddShip_ThrowsException_WhenFieldIsOccupied()
        {
            var _sut = Board.CreateBoard(5);
            var firstShip = Ship.CreateShip(Position.Create(0, 0), 2, Direction.Horizontal);
            var secondShip = Ship.CreateShip(Position.Create(1, 0), 1, Direction.Horizontal);
            _sut.AddShip(firstShip);
            Assert.Throws<NotAvailablePositionForVehicle>(() => _sut.AddShip(secondShip));
        }

        [Fact]
        public void AddShip_AddShipCorrectly_WhenFieldIsEmpty()
        {
            var _sut = Board.CreateBoard(5);
            var firstShip = Ship.CreateShip(Position.Create(0, 0), 2, Direction.Horizontal);
            var secondShip = Ship.CreateShip(Position.Create(2, 0), 1, Direction.Horizontal);
            _sut.AddShip(firstShip);
            _sut.AddShip(secondShip);
        }

        [Fact]
        public void AddShip_ThrowsException_WhenPositionIsNull()
        {
            var _sut = Board.CreateBoard(5);
            Assert.Throws<ArgumentNullException>(() => _sut.GetShipAtUnvisitedPositionAndMarkAsVisited(null));
        }

        [Fact]
        public void AddShip_ThrowsException_WhenPositionIsOutsideBoard()
        {
            var _sut = Board.CreateBoard(5);
            var firstPosition = Position.Create(5, 6);
            var secondPosition = Position.Create(6, 5);
            Assert.Throws<PositionOutsideTheBoard>(() => _sut.GetShipAtUnvisitedPositionAndMarkAsVisited(firstPosition));
            Assert.Throws<PositionOutsideTheBoard>(() => _sut.GetShipAtUnvisitedPositionAndMarkAsVisited(secondPosition));
        }

        [Fact]
        public void AddShip_ReturnsNothing_WhenFieldIsAlreadyVisited()
        {
            var _sut = Board.CreateBoard(5);
            var firstPosition = Position.Create(1, 1);
            var secondPosition = Position.Create(1, 1);
            _sut.GetShipAtUnvisitedPositionAndMarkAsVisited(firstPosition);
            Assert.Equal(Maybe<Ship>.Nothing, _sut.GetShipAtUnvisitedPositionAndMarkAsVisited(secondPosition));
        }
    }
}
